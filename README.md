# Halloween


Hello,


Stock audio - Free sound effects, loops and music.
 

There are no hidden costs or need to sign-up. 


"Halloween Creepy Sounds Pack"


Licensing Terms


Free for Commercial Use under following terms:

- If you use materials from sound pack in your commercial / non-commercial projects, set a proper backlink either to Orange Free Sounds or to this post.

- Publishing or hosting this Sound Pack, in whole or partially, on other websites and internet is not allowed.

- Changing Orange Free Sounds original download link to other websites (redirecting download to other websites) is not allowed.




http://www.orangefreesounds.com/